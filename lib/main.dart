import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart_with_bloc/bloc/product_bloc.dart';
import 'package:shopping_cart_with_bloc/data/product_repository.dart';
import 'package:shopping_cart_with_bloc/pages/cart.dart';
import 'package:shopping_cart_with_bloc/pages/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => ProductBloc(FakeRepository()),
        child: HomePage(),
      ),
      routes: {'/cart': (context) => CartPage()},
    );
  }
}

