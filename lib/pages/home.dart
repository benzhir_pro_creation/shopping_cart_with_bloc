
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart_with_bloc/bloc/bloc.dart';
import 'package:shopping_cart_with_bloc/bloc/product_bloc.dart';
import 'package:shopping_cart_with_bloc/data/model/product.dart';


class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

 @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<ProductBloc>(context)
      .add(GetProduct());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: Text("Home"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () => Navigator.pushNamed(context, '/cart'),
          )
        ],
      ),
      body: Container(
        child: BlocListener<ProductBloc, ProductState>(
          listener: (context, state) {
           if (state is ProductError) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                ),
              );
            }
          },
          child: BlocBuilder<ProductBloc, ProductState>(
        builder: (context, state) {
          if (state is ProductLoading) {
                return _loading();
              } else if (state is ProductLoaded) {
                return _gridView(state.products);
              } else if (state is ProductError) {
                return _errorText(state.message);
              }
        },
        ),
        )
      )
     
    );
  }

  _gridView(List<Product> products) {
    return GridView.builder(
        padding: EdgeInsets.all(8.0),
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount( crossAxisCount: 2, mainAxisSpacing: 8, crossAxisSpacing: 8, childAspectRatio: 0.8),
        itemBuilder: (context, index){
          return Card( child: Column( children: <Widget>[
              Image.network(products[index].imgUrl, height: 120, width: 120,),
              Text(products[index].title, style: TextStyle(fontWeight: FontWeight.bold),),
              Text("\$"+products[index].price.toString()),
              OutlineButton(
                    child: Text("Add"),
                   // onPressed: () => model.addProduct(products[index])
                    )
            ]));
        },
      );
  }

  _loading() {
     return Center(
      child: CircularProgressIndicator(),
    );
  }
  
  _errorText(String msg) {
     final productBloc = BlocProvider.of<ProductBloc>(context);
     return Center(
      child: FlatButton( 
        child: Text('Refresh'),
        onPressed: () => productBloc.add(GetProduct()),
        ),
    );
  }

}