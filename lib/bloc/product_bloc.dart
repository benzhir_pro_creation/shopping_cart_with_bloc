import 'package:shopping_cart_with_bloc/data/product_repository.dart';

import 'bloc.dart';
import 'package:bloc/bloc.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository productRepository;

  ProductBloc(this.productRepository);


  @override
  ProductState get initialState => ProductInitial();

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async*{
    yield ProductLoading();
    if(event is GetProduct){
       try {
        final products = await productRepository.fetchProduct();
        yield ProductLoaded(products);
      } on NetworkError {
        yield ProductError("Couldn't fetch products. try to refresh , just test error handling");
      }
    } else if (event is AddToCart){

    }
   
  }

}