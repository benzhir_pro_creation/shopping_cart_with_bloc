import 'package:equatable/equatable.dart';
import 'package:shopping_cart_with_bloc/data/model/product.dart';

abstract class ProductEvent extends Equatable{
  const ProductEvent();
}

class GetProduct extends ProductEvent {

  const GetProduct();

  @override
  List<Object> get props => null;

}

class AddToCart extends ProductEvent {

  final Product produt;

  const AddToCart({this.produt});

  @override
  List<Object> get props => null;

}