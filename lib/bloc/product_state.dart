import 'package:equatable/equatable.dart';
import 'package:shopping_cart_with_bloc/data/model/product.dart';

abstract class ProductState extends Equatable {
  const ProductState();
}

class ProductInitial extends ProductState {
  const ProductInitial();

  @override
  List<Object> get props => [];

}

class ProductLoading extends ProductState {
  const ProductLoading();

  @override
  List<Object> get props => [];

}

class ProductLoaded extends ProductState {
  final List<Product> products;
  const ProductLoaded(this.products);

  @override
  List<Object> get props => [];

}

class ProductError extends ProductState {
  final String message;
  const ProductError(this.message);

  @override
  List<Object> get props => [message];

}