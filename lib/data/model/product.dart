import 'dart:convert';

import 'package:equatable/equatable.dart';

class Product  extends Equatable{
  int id;
  String title;
  String imgUrl;
  double price;
  int qty;

  Product({
    this.id,
    this.title,
    this.imgUrl,
    this.price,
    this.qty,
  });
  

  Product copyWith({
    int id,
    String title,
    String imgUrl,
    double price,
    int qty,
  }) {
    return Product(
      id: id ?? this.id,
      title: title ?? this.title,
      imgUrl: imgUrl ?? this.imgUrl,
      price: price ?? this.price,
      qty: qty ?? this.qty,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'imgUrl': imgUrl,
      'price': price,
      'qty': qty,
    };
  }

  static Product fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Product(
      id: map['id'],
      title: map['title'],
      imgUrl: map['imgUrl'],
      price: map['price'],
      qty: map['qty'],
    );
  }

  String toJson() => json.encode(toMap());

  static Product fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() {
    return 'Product id: $id, title: $title, imgUrl: $imgUrl, price: $price, qty: $qty';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is Product &&
      o.id == id &&
      o.title == title &&
      o.imgUrl == imgUrl &&
      o.price == price &&
      o.qty == qty;
  }

  @override
  int get hashCode {
    return id.hashCode ^
      title.hashCode ^
      imgUrl.hashCode ^
      price.hashCode ^
      qty.hashCode;
  }

  @override
  List<Object> get props => [
    id,
    title,
    qty,
    price,
    imgUrl,
  ];
}

  List<Product> products = [
     Product(
        id: 1,
        title: "Apple",
        price: 20.0,
        imgUrl: "https://img.icons8.com/plasticine/2x/apple.png",
        qty: 1),
    Product(
        id: 2,
        title: "Banana",
        price: 40.0,
        imgUrl: "https://img.icons8.com/cotton/2x/banana.png",
        qty: 1),
    Product(
        id: 3,
        title: "Orange",
        price: 20.0,
        imgUrl: "https://img.icons8.com/cotton/2x/orange.png",
        qty: 1),
    Product(
        id: 4,
        title: "Melon",
        price: 40.0,
        imgUrl: "https://img.icons8.com/cotton/2x/watermelon.png",
        qty: 1),
    Product(
        id: 5,
        title: "Avocado",
        price: 25.0,
        imgUrl: "https://img.icons8.com/cotton/2x/avocado.png",
        qty: 1),
  ];