import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopping_cart_with_bloc/data/model/product.dart';

abstract class ProductRepository {
  Future<List<Product>> fetchProduct();
  Future<Product> addToCart(Product product);
}

class FakeRepository implements ProductRepository {

  @override
  Future<List<Product>> fetchProduct() {
     return Future.delayed( Duration(seconds: 1),
     (){
       final random = Random();
       if(!random.nextBool()){
         throw NetworkError();
       }
       return products;
    });
  }

  @override
  Future<Product> addToCart(Product product) {
    return Future.delayed( Duration(seconds: 1),
     () async{
       if(product.qty == 0){
         throw NetworkError();
       }
       return product;
    });
  }
}

class NetworkError extends Error {}
